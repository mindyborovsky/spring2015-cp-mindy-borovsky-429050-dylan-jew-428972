'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Match Schema
 */
var MatchSchema = new Schema({
	name: {
		type: String,
		default: '',
		trim: true
	},
	created: {
		type: Date,
		default: Date.now
	},
	player1: {
    type: Schema.ObjectId,
    ref: 'Player'
  },
  player2: {
    type: Schema.ObjectId,
    ref: 'Player'
  },
  player1Name: {
    type: String,
    default: ''
  },
  player2Name: {
    type: String,
    default: ''
  },
  player1Set1: {
    type: Number,
    default: 0
  },
  player1Set2: {
    type: Number,
    default: 0
  },
  player1Set3: {
    type: Number,
    default: 0
  },
  player2Set1: {
    type: Number,
    default: 0
  },
  player2Set2: {
    type: Number,
    default: 0
  },
  player2Set3: {
    type: Number,
    default: 0
  },
  player1forehandForcedErrors: {
    type: Number,
    default: 0
  },
  player1forehandUnforcedErrors: {
    type: Number,
    default: 0
  },
  player1BackhandForcedErrors: {
    type: Number,
    default: 0
  },
  player1BackhandUnforcedErrors: {
    type: Number,
    default: 0
  },
  player1ServeAttempts: {
    type: Number,
    default: 0
  },
  player1FirstServesIn: {
    type: Number,
    default: 0
  },
  player1SecondServesIn: {
    type: Number,
    default: 0
  },
  player1BackhandWinners: {
    type: Number,
    default: 0
  },
  player1ForehandWinners: {
    type: Number,
    default: 0
  },
  player1PointsWonAtNet: {
    type: Number,
    default: 0
  },
  player1Aces: {
    type: Number,
    default: 0
  },
  player1DoubleFaults: {
    type: Number,
    default: 0
  },
  player2forehandForcedErrors: {
    type: Number,
    default: 0
  },
  player2forehandUnforcedErrors: {
    type: Number,
    default: 0
  },
  player2BackhandForcedErrors: {
    type: Number,
    default: 0
  },
  player2BackhandUnforcedErrors: {
    type: Number,
    default: 0
  },
  player2ServeAttempts: {
    type: Number,
    default: 0
  },
  player2FirstServesIn: {
    type: Number,
    default: 0
  },
  player2SecondServesIn: {
    type: Number,
    default: 0
  },
  player2BackhandWinners: {
    type: Number,
    default: 0
  },
  player2ForehandWinners: {
    type: Number,
    default: 0
  },
  player2PointsWonAtNet: {
    type: Number,
    default: 0
  },
  player2Aces: {
    type: Number,
    default: 0
  },
  player2DoubleFaults: {
    type: Number,
    default: 0
  },
  matchWinner: {
    type: Schema.ObjectId,
    ref: 'Player'
  },
  matchLoser: {
    type: Schema.ObjectId,
    ref: 'Player'
  },
	user: {
		type: Schema.ObjectId,
		ref: 'User'
	}
});

mongoose.model('Match', MatchSchema);