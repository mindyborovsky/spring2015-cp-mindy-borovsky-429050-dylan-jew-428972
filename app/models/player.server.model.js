'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Player Schema
 */
var PlayerSchema = new Schema({
	name: {
		type: String,
		default: '',
		required: 'Please fill Player name',
		trim: true
	},
	created: {
		type: Date,
		default: Date.now
	},
	  wins: {
    type: Number,
    default: 0
  },
  losses: {
    type: Number,
    default: 0
  },
  serveAttempts: {
    type: Number,
    default: 0
  },
  firstServesIn: {
    type: Number,
    default: 0
  },
  secondServesIn: {
    type: Number,
    default: 0
  },
  backhandWinners: {
    type: Number,
    default: 0
  },
  forehandWinners: {
    type: Number,
    default: 0
  },
  forehandUnforcedErrors: {
    type: Number,
    default: 0
  },
  backhandUnforcedErrors: {
    type: Number,
    default: 0
  },
  forehandForcedErrors: {
    type: Number,
    default: 0
  },
  backhandForcedErrors: {
    type: Number,
    default: 0
  },
  pointsWonAtNet: {
    type: Number,
    default: 0
  },
  aces: {
    type: Number,
    default: 0
  },
  doubleFaults: {
    type: Number,
    default: 0
  },
	user: {
		type: Schema.ObjectId,
		ref: 'User'
	}
});

mongoose.model('Player', PlayerSchema);