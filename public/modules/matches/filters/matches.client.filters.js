'use strict';
var mod = angular.module('matches');


mod.filter('serving', function() {
  return function(input) {
    return input ? '\u25C9' : '\u0020';
  };
});

mod.filter('gameScoreFilter', function(){
    return function(input, scope) {
        if (!scope.inTiebreak){
            if (input === 0){
                return 'Love';
             }
            else if (input === 1){
                return '15';
            }
            else if (input === 2){
                return '30';
            } else if (input === 3){
                return '40';
            } else if (input === 4){
                return 'Deuce';
            } else if (input === 15){
                return 'Ad';
            }
            else if (input === 14){
                return '-';
            }
        } else {
            return input;
        }
    };
});

mod.filter('WonGameFilter', function() {
    return function(input, scope, won) {
        var check = won ? scope.match.matchWinner : scope.match.matchLoser;
        if (input === check) {
            return '\u2713';
        } else {
            return '';
        }
    };
});

mod.filter('CheckThirdSetFilter', function() {
    return function(input, scope) {
        var check = scope.match.player1Set3 === 0 && scope.match.player2Set3 === 0;
        if (!check) {
            return input;
        } else {
            return '';
        }
    };
});