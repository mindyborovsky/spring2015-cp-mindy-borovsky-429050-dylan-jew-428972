'use strict';

// Matches controller
angular.module('matches').controller('MatchesController', ['$scope', '$stateParams', '$location', 'Authentication', 'Matches', 'Players',
	function($scope, $stateParams, $location, Authentication, Matches, Players) {
		$scope.authentication = Authentication;


		// Create new Match
		$scope.create = function() {
			// Create new Match object should use existing player
			// check who won match
			var match = new Matches ({
				name: Date.now(),
				player1: $scope.players[$scope.player1Index]._id,
				player2: $scope.players[$scope.player2Index]._id,
				player1Name: $scope.player1.name,
				player2Name: $scope.player2.name,
				player1Set1: $scope.player1.set1,
				player1Set2: $scope.player1.set2,
				player1Set3: $scope.player1.set3,
				player2Set1: $scope.player2.set1,
				player2Set2: $scope.player2.set2,
				player2Set3: $scope.player2.set3,
				player1forehandForcedErrors: $scope.player1forehandForcedErrors,
				player1forehandUnforcedErrors: $scope.player1forehandUnforcedErrors,
				player1BackhandForcedErrors: $scope.player1BackhandForcedErrors,
				player1BackhandUnforcedErrors: $scope.player1BackhandUnforcedErrors,
				player1ServeAttempts: $scope.player1ServeAttempts,
				player1FirstServesIn: $scope.player1FirstServesIn,
				player1SecondServesIn: $scope.player1SecondServesIn,
				player1BackhandWinners: $scope.player1BackhandWinners,
				player1ForehandWinners: $scope.player1ForehandWinners,
				player1PointsWonAtNet: $scope.player1PointsWonAtNet,
				player1Aces: $scope.player1Aces,
				player1DoubleFaults: $scope.player1DoubleFaults,
				player2forehandForcedErrors: $scope.player2forehandForcedErrors,
				player2forehandUnforcedErrors: $scope.player2forehandUnforcedErrors,
				player2BackhandForcedErrors: $scope.player2BackhandForcedErrors,
				player2BackhandUnforcedErrors: $scope.player2BackhandUnforcedErrors,
				player2ServeAttempts: $scope.player2ServeAttempts,
				player2FirstServesIn: $scope.player2FirstServesIn,
				player2SecondServesIn: $scope.player2SecondServesIn,
				player2BackhandWinners: $scope.player2BackhandWinners,
				player2ForehandWinners: $scope.player2ForehandWinners,
				player2PointsWonAtNet: $scope.player2PointsWonAtNet,
				player2Aces: $scope.player2Aces,
				player2DoubleFaults: $scope.player2DoubleFaults,
				matchWinner: $scope.matchWinner,
				matchLoser: $scope.matchLoser
			});

 			//Redirect after save
			match.$save(function(response) {

				// save new player
				$scope.savePlayer1();
				$scope.savePlayer2();
				// move to match view
				$location.path('matches/' + response._id);
				// Clear form fields
				$scope.name = '';
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Remove existing Match
		$scope.remove = function(match) {
			if ( match ) {
				match.$remove();

				for (var i in $scope.matches) {
					if ($scope.matches [i] === match) {
						$scope.matches.splice(i, 1);
					}
				}
			} else {
				$scope.match.$remove(function() {
					$location.path('matches');
				});
			}
		};

		// Update existing Match
		$scope.update = function() {
			var match = $scope.match;

			match.$update(function() {
				$location.path('matches/' + match._id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		$scope.findPlayers = function() {
			$scope.players = Players.query();
		};

		$scope.savePlayer1 = function() {
			var player = $scope.players[$scope.player1Index];

				if ($scope.matchWinner === player._id) {
					player.wins += 1;
				} else {
					player.losses += 1;
				}

				player.forehandForcedErrors += $scope.player1forehandForcedErrors;
				player.forehandUnforcedErrors += $scope.player1forehandUnforcedErrors;
				player.backhandForcedErrors += $scope.player1BackhandForcedErrors;
				player.backhandUnforcedErrors += $scope.player1BackhandUnforcedErrors;
				player.serveAttempts += $scope.player1ServeAttempts;
				player.firstServesIn += $scope.player1FirstServesIn;
				player.secondServesIn += $scope.player1SecondServesIn;
				player.backhandWinners += $scope.player1BackhandWinners;
				player.forehandWinners += $scope.player1ForehandWinners;
				player.pointsWonAtNet += $scope.player1PointsWonAtNet;
				player.aces += $scope.player1Aces;
				player.doubleFaults += $scope.player1DoubleFaults;

			player.$update(function() {
				console.log('successful save');
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		$scope.savePlayer2 = function() {
			var player = $scope.players[$scope.player2Index];

				if ($scope.matchWinner === player._id) {
					player.wins += 1;
				} else {
					player.losses += 1;
				}

				player.forehandForcedErrors += $scope.player2forehandForcedErrors;
				player.forehandUnforcedErrors += $scope.player2forehandUnforcedErrors;
				player.backhandForcedErrors += $scope.player2BackhandForcedErrors;
				player.backhandUnforcedErrors += $scope.player2BackhandUnforcedErrors;
				player.serveAttempts += $scope.player2ServeAttempts;
				player.firstServesIn += $scope.player2FirstServesIn;
				player.secondServesIn += $scope.player2SecondServesIn;
				player.backhandWinners += $scope.player2BackhandWinners;
				player.forehandWinners += $scope.player2ForehandWinners;
				player.pointsWonAtNet += $scope.player2PointsWonAtNet;
				player.aces += $scope.player2Aces;
				player.doubleFaults += $scope.player2DoubleFaults;


			player.$update(function() {
				console.log('successful save');
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};


		$scope.startMatch = function() {
			//check if players selected
			$scope.matchStarted = true;
			$scope.player1Serving = true;
			$scope.player1 = new Players({
				name: $scope.players[$scope.player1Index].name,
				set1: 0,
				set2: 0,
				set3: 0,
				gameScore: 0
			});
			$scope.player2 = new Players({
				name: $scope.players[$scope.player2Index].name,
				set1: 0,
				set2: 0,
				set3: 0,
				gameScore: 0
			});
			//initialize variables
			$scope.player1forehandForcedErrors = 0;
			$scope.player1forehandUnforcedErrors = 0;
			$scope.player1BackhandForcedErrors = 0;
			$scope.player1BackhandUnforcedErrors = 0;
			$scope.player1ServeAttempts = 0;
			$scope.player1FirstServesIn = 0;
			$scope.player1SecondServesIn = 0;
			$scope.player1BackhandWinners = 0;
			$scope.player1ForehandWinners = 0;
			$scope.player1PointsWonAtNet = 0;
			$scope.player1Aces = 0;
			$scope.player1DoubleFaults = 0;
			$scope.player2forehandForcedErrors = 0;
			$scope.player2forehandUnforcedErrors = 0;
			$scope.player2BackhandForcedErrors = 0;
			$scope.player2BackhandUnforcedErrors = 0;
			$scope.player2ServeAttempts = 0;
			$scope.player2FirstServesIn = 0;
			$scope.player2SecondServesIn = 0;
			$scope.player2BackhandWinners = 0;
			$scope.player2ForehandWinners = 0;
			$scope.player2PointsWonAtNet = 0;
			$scope.player2Aces = 0;
			$scope.player2DoubleFaults = 0;

			$scope.finishedServe = false;
			$scope.finishedMatch = false;
			$scope.currentSet = 1;
			$scope.inTiebreak = false;
		};

		$scope.firstServeIn = function () {
			if ($scope.player1Serving) {
				$scope.player1FirstServesIn += 1;
				$scope.player1ServeAttempts += 1;
			} else {
				$scope.player2FirstServesIn += 1;
				$scope.player2ServeAttempts += 1;
			}
			$scope.finishedServe = true;
		};

		$scope.secondServeIn = function(){
			if ($scope.player1Serving) {
				$scope.player1SecondServesIn += 1;
			} else {
				$scope.player2SecondServesIn += 1;
			}
			$scope.finishedServe = true;
		};

		$scope.ace = function(){
			if ($scope.player1Serving) {
				$scope.player1Aces += 1;
				$scope.player1ServeAttempts += 1;
				$scope.player1FirstServesIn += 1;
				$scope.updateScore($scope.player1.name);
			} else {
				$scope.player2Aces += 1;
				$scope.player2ServeAttempts += 1;
				$scope.player2FirstServesIn += 1;
				$scope.updateScore($scope.player2.name);
			}
		};

		$scope.doubleFault = function(){
			if ($scope.player1Serving) {
				$scope.player1DoubleFaults += 1;
				$scope.player1ServeAttempts += 2;
				$scope.updateScore($scope.player2.name);
			} else {
				$scope.player2DoubleFaults += 1;
				$scope.player2ServeAttempts += 2;
				$scope.updateScore($scope.player1.name);
			}
		};

		$scope.forehandForcedError1 = function(){
			$scope.player1forehandForcedErrors += 1;
			$scope.updateScore($scope.player2.name);
		};

		$scope.forehandForcedError2 = function(){
			$scope.player2forehandForcedErrors += 1;
			$scope.updateScore($scope.player1.name);
		};

		$scope.forehandUnforcedError1 = function(){
			$scope.player1forehandUnforcedErrors += 1;
			$scope.updateScore($scope.player2.name);
		};

		$scope.forehandUnforcedError2 = function(){
			$scope.player2forehandUnforcedErrors += 1;
			$scope.updateScore($scope.player1.name);
		};


		$scope.backhandForcedError1 = function(){
			$scope.player1BackhandForcedErrors += 1;
			$scope.updateScore($scope.player2.name);
		};

		$scope.backhandForcedError2 = function(){
			$scope.player2BackhandForcedErrors += 1;
			$scope.updateScore($scope.player1.name);
		};

		$scope.backhandUnforcedError1 = function(){
			$scope.player1BackhandUnforcedErrors += 1;
			$scope.updateScore($scope.player2.name);
		};

		$scope.backhandUnforcedError2 = function(){
			$scope.player2BackhandUnforcedErrors += 1;
			$scope.updateScore($scope.player1.name);
		};

		$scope.forehandWinner1 = function(){
			$scope.player1ForehandWinners += 1;
			$scope.updateScore($scope.player1.name);
		};

		$scope.forehandWinner2 = function(){
			$scope.player2ForehandWinners += 1;
			$scope.updateScore($scope.player2.name);
		};

		$scope.backhandWinner1 = function(){
			$scope.player1BackhandWinners += 1;
			$scope.updateScore($scope.player1.name);
		};

		$scope.backhandWinner2 = function(){
			$scope.player2BackhandWinners += 1;
			$scope.updateScore($scope.player2.name);
		};

		$scope.net1 = function(){
			$scope.player1PointsWonAtNet += 1;
			$scope.updateScore($scope.player1.name);
		};

		$scope.net2 = function(){
			$scope.player2PointsWonAtNet += 1;
			$scope.updateScore($scope.player2.name);
		};

		$scope.updateScore = function(name) {
			if (name === $scope.player1.name){
				$scope.player1.gameScore += 1;
			} else {
				$scope.player2.gameScore += 1;
			}

			if ($scope.inTiebreak){
				if (($scope.player1.gameScore + $scope.player2.gameScore)%2 === 1){
					$scope.player1Serving = !$scope.player1Serving;
				}


				if ($scope.player1.gameScore >= 7){
					if ($scope.player1.gameScore >= $scope.player2.gameScore + 2){
						//won
						if ($scope.currentSet === 1){
							$scope.player1.set1 += 1;
							$scope.inTiebreak = false;
							$scope.player1.gameScore = 0;
							$scope.player2.gameScore = 0;
							$scope.currentSet += 1;
						} else if ($scope.currentSet === 2){
							$scope.player1.set2 += 1;
							$scope.inTiebreak = false;
							$scope.player1.gameScore = 0;
							$scope.player2.gameScore = 0;
							$scope.currentSet += 1;
							if ($scope.player1.set1 > $scope.player2.set1) {
								$scope.finishedMatch = true;
							}
						} else if ($scope.currentSet === 3){
							$scope.player1.set3 += 1;
							$scope.inTiebreak = false;
							$scope.finishedMatch = true;
						}
					}
				}
				else if ($scope.player2.gameScore >= 7){
					if ($scope.player2.gameScore >= $scope.player1.gameScore + 2){
						//won
						if ($scope.currentSet === 1){
							$scope.player2.set1 += 1;
							$scope.inTiebreak = false;
							$scope.player1.gameScore = 0;
							$scope.player2.gameScore = 0;
							$scope.currentSet += 1;
						} else if ($scope.currentSet === 2){
							$scope.player2.set2 += 1;
							$scope.inTiebreak = false;
							$scope.player1.gameScore = 0;
							$scope.player2.gameScore = 0;
							$scope.currentSet += 1;
							if ($scope.player2.set1 > $scope.player1.set1) {
								$scope.finishedMatch = true;
							}
						} else if ($scope.currentSet === 3){
							$scope.player2.set3 += 1;
							$scope.inTiebreak = false;
							$scope.finishedMatch = true;
						}
					}
				}

			} else {
				if ($scope.player1.gameScore === $scope.player2.gameScore && $scope.player1.gameScore >= 3 && $scope.player2.gameScore >= 3){
					$scope.player1.gameScore = 4;
					$scope.player2.gameScore = 4;
				}
				else if ($scope.player1.gameScore >= 4 && $scope.player1.gameScore >= $scope.player2.gameScore + 2){
					//player 1 won the game
					if ($scope.currentSet === 1){
						$scope.player1.set1 += 1;
						$scope.player1.gameScore = 0;
						$scope.player2.gameScore = 0;
						$scope.player1Serving = !$scope.player1Serving;
						//check if need to start a tiebreak
						if ($scope.player1.set1 === 6 && $scope.player2.set1 === 6){
							$scope.inTiebreak = true;
						}

						//handle set logic
						if ($scope.player1.set1 === 6 && $scope.player1.set1 >= $scope.player2.set1 + 2){
							$scope.currentSet = 2;
						}
					} else if ($scope.currentSet === 2){
						$scope.player1.set2 += 1;
						$scope.player1.gameScore = 0;
						$scope.player2.gameScore = 0;
						$scope.player1Serving = !$scope.player1Serving;
						//check if need to start a tiebreak
						if ($scope.player1.set2 === 6 && $scope.player2.set2 === 6){
							$scope.inTiebreak = true;
						}

						//handle set logic
						if ($scope.player1.set2 === 6 && $scope.player1.set2 >= $scope.player2.set2 + 2){
							if ($scope.player1.set1 < $scope.player2.set1){
								//split sets
								$scope.currentSet = 3;
							} else {
								//end match
								$scope.finishedMatch = true;
							}
						}


					} else if ($scope.currentSet === 3){
						//end match if set is won
						$scope.player1.set3 += 1;
						$scope.player1.gameScore = 0;
						$scope.player2.gameScore = 0;
						$scope.player1Serving = !$scope.player1Serving;
						//check if need to start a tiebreak
						if ($scope.player1.set3 === 6 && $scope.player2.set3 === 6){
							$scope.inTiebreak = true;
						}

						if ($scope.player1.set3 === 6 && $scope.player1.set3 >= $scope.player2.set3 + 2){
							$scope.finishedMatch = true;
						}
					}

				}
				else if ($scope.player2.gameScore >= 4 && $scope.player2.gameScore >= $scope.player1.gameScore + 2){
					//player 2 won the game
					if ($scope.currentSet === 1){
						$scope.player2.set1 += 1;
						$scope.player1.gameScore = 0;
						$scope.player2.gameScore = 0;
						$scope.player1Serving = !$scope.player1Serving;
						//check if need to start a tiebreak
						if ($scope.player1.set1 === 6 && $scope.player2.set1 === 6){
							$scope.inTiebreak = true;
						}

						if ($scope.player2.set1 === 6 && $scope.player2.set1 >= $scope.player1.set1 + 2){
							$scope.currentSet = 2;
						}
					} else if ($scope.currentSet === 2){
						$scope.player2.set2 += 1;
						$scope.player1.gameScore = 0;
						$scope.player2.gameScore = 0;
						$scope.player1Serving = !$scope.player1Serving;


						if ($scope.player1.set2 === 6 && $scope.player2.set2 === 6){
							$scope.inTiebreak = true;
						}
						//handle set logic
						if ($scope.player2.set2 === 6 && $scope.player2.set2 >= $scope.player1.set2 + 2){
							if ($scope.player2.set1 < $scope.player1.set1){
								//split sets
								$scope.currentSet = 3;
							} else {
								//end match
								$scope.finishedMatch = true;
							}
						}
					} else if ($scope.currentSet === 3){
						$scope.player2.set3 += 1;
						$scope.player1.gameScore = 0;
						$scope.player2.gameScore = 0;
						$scope.player1Serving = !$scope.player1Serving;
						//check if need to start a tiebreak

						if ($scope.player1.set3 === 6 && $scope.player2.set3 === 6){
							$scope.inTiebreak = true;
						}

						if ($scope.player2.set3 === 6 && $scope.player2.set3 >= $scope.player1.set3 + 2){
							$scope.finishedMatch = true;
						}

					}
				}
				else if ($scope.player1.gameScore > $scope.player2.gameScore && $scope.player1.gameScore >= 3 && $scope.player2.gameScore >= 3){
					$scope.player1.gameScore = 15; // 15 ad
					$scope.player2.gameScore = 14;//14 represents empty
				}
				else if ($scope.player1.gameScore < $scope.player2.gameScore && $scope.player1.gameScore >= 3 && $scope.player2.gameScore >= 3){
					$scope.player2.gameScore = 15;// 15 ad
					$scope.player1.gameScore = 14;//14 represents empty
				}
			}

			$scope.finishedServe = false;
			if ($scope.finishedMatch) {
				$scope.matchComplete();
			}
		};

		// Find a list of Matches
		$scope.find = function() {
			$scope.matches = Matches.query();
		};

		$scope.matchComplete = function() {
			// find winner and set
			if ($scope.player1.set1 > $scope.player2.set1 && $scope.player1.set2 > $scope.player2.set2) {
				$scope.matchWinner = $scope.players[$scope.player1Index]._id;
				$scope.matchLoser = $scope.players[$scope.player2Index]._id;
			} else if ($scope.player1.set1 < $scope.player2.set1 && $scope.player1.set2 < $scope.player2.set2) {
				$scope.matchLoser = $scope.players[$scope.player1Index]._id;
				$scope.matchWinner = $scope.players[$scope.player2Index]._id;
			} else if ($scope.player1.set3 > $scope.player2.set3) {
				$scope.matchWinner = $scope.players[$scope.player1Index]._id;
				$scope.matchLoser = $scope.players[$scope.player2Index]._id;
			} else {
				$scope.matchLoser = $scope.players[$scope.player1Index]._id;
				$scope.matchWinner = $scope.players[$scope.player2Index]._id;
			}


		};

		// Find existing Match
		$scope.findOne = function() {
			$scope.match = Matches.get({
				matchId: $stateParams.matchId
			});
		};
	}
]);